# Optimize decorating your Christmas tree #

Sources = [Formules treegonometry de l'université de Sheffield](http://www.shef.ac.uk/polopoly_fs/1.227815!/image/formulas.jpg)

## Formules treegonometry convert to C ##

```
#!c

// Return the number of objects
int calObjects( int a_objects )
{
   return a_objects = (sqrt (17) / 20) * heightTree;
}

// Return the height of the star
int calHeightStar( int b_height )
{
   return b_height = heightTree / 10;
}

// Return the length of the strings
int calWidthGarlands( int c_width )
{
   return c_width = ((13 * PI) / 8) * heightTree;
}

// Return the length of the electric garlands
int calWidthGarlandsElectrics( int d_width )
{
   return d_width = PI * heightTree; 
}

```

## Build ##

```
#!sh

gcc main.c functions.c -o file_output -lm


```
## Launch ##

```
#!sh

./file_output

```

## Copyright and License ##

GNU GPLv3
